<?php 
require_once '../database.php';

// Fetch user details
$sql = "SELECT * FROM recruit_application";
$res = $db->query($sql);
$count = mysqli_num_rows($res);

$skill = array();

$resume_file_path  = "http://".$_SERVER['SERVER_NAME']."/career/resume_application/";

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/admin.css">

    <title>Career@alegralabs</title>
  </head>
  <body>

    <header id="header">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="logo logo-navbar">
              <a href="index.html">
                 <img src="http://alegralabs.com/source/img/logo-black.png">
              </a>
            </div>
            <div class="language">
              <!--<span class="hidden">
                <a href="Denmark.html">
                  DE
                </a>
              </span>
              <span >
                <a href="index.html">
                  EN
                </a>
              </span> -->
            </div>
            <div class="menu-icon">
              <div class="hamburg-menu">
                  <div class="hamburger" id="hamburger-1">
                      <img src="http://alegralabs.com/source/img/menu-black.svg">
                  </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </header>

    <section class="admin-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 main-data-table">
                    <div class="col-lg-12 text-center">
                        <h2 class="upper">Candidate List</h2>
                        <p>Total Applicants : <span><?=$count;?></span></p>
                    </div>
                    <table id="applicant_data_table" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>SL No</th>
                                <th>Full Name</th>
                                <th>Email Id</th>
                                <th>Phone No</th>
                                <th>Address</th>
                                <th>Skills/Yr</th>
                                <th>Training</th>
                                <th>Why Hire Me</th>
                                <th>Discipline Grade</th>
                                <th>Working Grade</th>
                                <th>Resume Link</th>
                                <th>Agreement Accepted</th>
                                <th>Application Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $i = 1;
                            while($userdata = mysqli_fetch_assoc($res)): 
                                $skills = $userdata['skills'];
                                $years = $userdata['years'];
                                $skillsarr = explode(",", $skills);
                                $yearsarr = explode(",", $years);

                                $fileurl = $resume_file_path.$userdata['resume'];
                            ?>

                            <tr id="table-row-<?=$userdata['id'];?>">
                                <td><p><?=$i;?></td>
                                <td><p><?=$userdata['full_name'];?></p></td>
                                <td><p><?=$userdata['email'];?></p></td>
                                <td><p><?=$userdata['phone'];?></p></td>
                                <td><p><?=$userdata['address'];?></p></td>
                                <td class="skill">
                                    <ul>
                                        <?php
                                            for ($j=0; $j < count($skillsarr); $j++) {
                                                echo  '<li><span class="skill-name">'.$skillsarr[$j].'</span>'. '<span class="skill-yr">' .$yearsarr[$j].'</li>';
                                            }
                                        ?>  
                                    </ul>
                                </td>
                                <td><p><?=$userdata['training'];?></p></td>
                                <td><p><?=$userdata['why_hire_me'];?></p></td>
                                <td><p><?=$userdata['discipline_grade'];?></p></td>
                                <td><p><?=$userdata['working_grade'];?></p></td>
                                <td><p><a href="https://docs.google.com/gview?url=<?=$fileurl;?>" target="_blank"><?=$fileurl;?> </a></p></td>
                                <td><p><?=$userdata['agreement'];?></p></td>
                                <td><p><?=$userdata['apply_date'];?></p></td>
                                <td class="action-btn">
                                    <a title="Delete Data" class="delete" value="<?=$userdata['id'];?>"><img src="../img/delete.svg" height="20px"></a> 
                                    <a href="https://docs.google.com/gview?url=<?=$fileurl;?>" target="_blank" title="View Resume"><img src="../img/view.svg" height="20px"></a>
                                    <a title="Reply" data-toggle="modal" data-target="#reply-modal"><img src="../img/reply.svg" height="20px"></a>
                                </td>
                            </tr>
                            <?php 
                                $i++; 
                                endwhile;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section class="preloader">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 preloader-inner">
                    <svg class="loader" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 340 340">
                         <circle cx="170" cy="170" r="160" stroke="#E2007C"/>
                         <circle cx="170" cy="170" r="135" stroke="#ffffff"/>
                         <circle cx="170" cy="170" r="110" stroke="#E2007C"/>
                         <circle cx="170" cy="170" r="85" stroke="#ffffff"/>
                    </svg>
                </div>
            </div>
        </div>
    </section>

    <!-- Reply Modal -->
    <div class="modal fade" id="reply-modal" tabindex="-1" role="dialog" aria-labelledby="reply-modalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="reply-modalLabel">Send Message </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-inline">
                    <label class="my-1 mr-2 col-lg-4" for="subject">Subject</label>
                    <input type="text" class="form-control col-lg-7" name="subject" value="Application Form Submit">
                </div>
                <div class="form-inline">
                    <label class="my-1 mr-2 col-lg-4" for="subject">Message</label>
                    <textarea class="form-control col-lg-7" rows="5">Thank You For Submit Your Message</textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Send</button>
            </div>
        </div>
      </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/dataTables.bootstrap4.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#applicant_data_table').DataTable();

            $('.delete').click(function(){                
                var id = $(this).attr("value");
                var data_id = JSON.stringify({deleteid: id});

                $.ajaxSetup({
                    url: "delete-data.php",
                    data: data_id,
                    async: true,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function(){
                        $(".preloader").show();
                    },
                    complete: function(){
                       $(".preloader").hide();
                    }
                });
                $.post()
                .done(function(response) {
                    
                    console.log(response)
                    var res = JSON.parse(response);
                    var status = res['status'];
                    var message = res['message'];

                    if ( status == 'success' ){
                        $('#delete-message').show();
                        $('#table-row-'+id).fadeOut();
                    }
                    else{
                        location.reload(true);
                    }
                })
                .fail(function() {
                    alert('failed to process');
                })
                return false;
            });

        } );
    </script>   

  </body>
</html>