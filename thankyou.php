<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/career.css">

    <title>Career@alegralabs</title>
  </head>
  <body>

    <header id="header">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="logo logo-navbar">
              <a href="http://www.alegralabs.com/">
                 <img src="http://alegralabs.com/source/img/logo-black.png">
              </a>
            </div>
            <div class="language">
              <!--<span class="hidden">
                <a href="Denmark.html">
                  DE
                </a>
              </span>
              <span >
                <a href="index.html">
                  EN
                </a>
              </span> -->
            </div>
            <div class="menu-icon">
              <div class="hamburg-menu">
                  <div class="hamburger" id="hamburger-1">
                      <img src="http://alegralabs.com/source/img/menu-black.svg">
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    <section class="main-section">
        <div class="container">
            <div style="height: 100vh;width: 100%;display: flex;align-items: center;" class="row">
                <div class="col-lg-8 offset-lg-2 alert alert-success" style="text-align: center; padding: 30px 50px;border: 1px solid #000;">
                    <h3> Your application has been submitted<strong> successfully</strong>.</h3>
                    <p style="margin-top: 10px;margin-bottom: 10px; padding: 0 50px;">We have received your application. Please be aware that we will not reply to unsuccessful applicants.</br>Please be assured that your application will be reviewed promptly and if it is of interest, you will be contacted within 7 working days.</p>
                    <a style="margin-top:10px" class="btn btn-primary submit-btn btn-sm" href="#">Close</a>
                </div>
            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  </body>
</html>