<?php 
require_once '../core/database.php';
require("PHPMailer_5.2.0/class.phpmailer.php");

date_default_timezone_set('Asia/Kolkata');



function sanitize($dirty){
    global $db;
    return trim(mysqli_real_escape_string($db, $dirty));
}


$error = array();
$status = $message = "";
$site_path  = "http://".$_SERVER['SERVER_NAME']."/suraj/career";

$params = file_get_contents('php://input');
$json = json_decode($params);

$full_name = (isset($_POST['full_name'])? $_POST['full_name'] : '' );
$email_id = (isset($_POST['email_id'])? $_POST['email_id'] : '' );
$phone_no = (isset($_POST['phone_no'])? $_POST['phone_no'] : '' );
$address = (isset($_POST['address'])? $_POST['address'] : '' );
$skill = (isset($_POST['skill_json'])? $_POST['skill_json'] : '' );
$training = (isset($_POST['training'])? $_POST['training'] : '' );
$hire_me_reason = (isset($_POST['hire_me_reason'])? $_POST['hire_me_reason'] : '' );
$discipline = (isset($_POST['discipline'])? $_POST['discipline'] : '' );
$working = (isset($_POST['working'])? $_POST['working'] : '' );
$agree = (isset($_POST['policy'])? $_POST['policy'] : '' );
$resumeName =  ( isset($_FILES['resume']['name'])? $_FILES['resume']['name'] : '' );
$resumeSize = ( isset($_FILES['resume']['size'])? $_FILES['resume']['size'] : '' );
$tmpLoc =  ( isset($_FILES['resume']['tmp_name'])? $_FILES['resume']['tmp_name'] : '' );


$skillarray = json_decode($skill);

if (empty($full_name)) {
	$error['full_name'] = 'Enter your full name.';
}else{
	if( !preg_match('/^[a-zA-Z\s.]{3,255}$/', $full_name) ) {
      $error['full_name'] = 'Name should be of max. 255 characters. Only Alphabets, space and dot allowed.';
    }
}

if (empty($email_id)) {
	$error['email_id'] = 'Enter your email address.';
}else{
	if (!filter_var($email_id, FILTER_VALIDATE_EMAIL)) {
      $error['email_id'] = 'You must enter a valid email address.'; 
    }
}

if (empty($phone_no)) {
	$error['phone_no'] = 'Enter your phone number.';
}else{
	if( !preg_match('/^[\[\(\0-9\s.\_\-\+\/\)\]]{3,15}$/', $phone_no) ) {
      $error['phone_no'] = 'Contact No. must be of max. 15 characters. Allowed special characters are (+,.,/,-,space,(),[]).';
    }
}

if (empty($address)) {
	$error['address'] = 'Enter your full address details.';	
}else{
	if (strlen($address) > 250 ) {
		$error['address'] = 'Enter maximum 250 characters';
	}
}

if (sizeof($skillarray) == 0) {
	$error['skills'] = 'Please select your skills.';
}

if (empty($hire_me_reason)) {
	$error['hire_me_reason'] = 'Please fill why company hire you.';
}else{
	if (strlen($address) > 510 ) {
		$error['hire_me_reason'] = 'Enter maximum 500 characters';
	}
}

if (empty($discipline)) {
	$error['discipline'] = 'Select your discipline grade.';	
}else{
	if ($discipline == 0) {
		$error['discipline'] = 'Discipline grade cant be zoro.';	
	}
}

if (empty($working)) {
	$error['working'] = 'Select your attendence and working grade.';	
}else{
	if ($working == 0) {
		$error['working'] = 'Attendence and working grade cant be zoro.';	
	}
}

if (empty($agree)) {
	$error['agree'] = 'You must agree to work for at least 2 years.';	
}

if (empty($resumeName)) {
	$error['resume'] = 'Upload your resume.';		
}else{
	$resumenamearray = explode('.', $resumeName);
	$fileName = isset($resumenamearray[0]) ? $resumenamearray[0] : '';
	$fileExt = 	isset($resumenamearray[1]) ? $resumenamearray[1] : '';
	$allowed = array('txt','doc','docx','pdf');
	
	if (!in_array($fileExt, $allowed)) {
		$error['resume'] = 'Resume extension must be a txt, doc, docx, or pdf.';
	}else{
		if ($resumeSize > 1048576) {
			$error['resume'] = 'Resume size cant be more than 1mb. ';
		}
	}
}


if (!empty($error)) {
	$status = "fail";
    $message = "Please check your fields properly then submit the form";
	
}else{

	$t = microtime(true);
    $micro = sprintf("%06d",($t - floor($t)) * 100000000);
    $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
    $current_date = $d->format('Y-m-d\TH:i:s.u');

    $resumeName = $current_date.$resumeName;

	$uploadPath =  '../resume_application/'.$resumeName;
	$resumepath = '../resume_application/'.$resumeName;

	$resumepathlink = $site_path.'/resume_application/'.$resumeName;

	move_uploaded_file($tmpLoc, $uploadPath);
	
	$address = sanitize($address); 
	$skill = sanitize($skill);
	$hire_me_reason = sanitize($hire_me_reason);
	$discipline = sanitize($discipline);
	$working = sanitize($working);

	$insertApplication = "INSERT INTO recruit_application ( full_name, email, phone, address, skills, training, why_hire_me, discipline_grade, working_grade, resume, agreement, apply_date ) values ('$full_name','$email_id','$phone_no','$address','$skill','$training','$hire_me_reason','$discipline','$working','$resumepathlink','$agree', '$current_date' ) " ;

	$applicationQuery = $db->query($insertApplication);

	if ($applicationQuery) {
		$status = "success";
	    $message = "We have recieve your application, Thank You!.";
	    mysqli_close($db);

		  //   $to = 'jay@alegralabs.com';
		  //   $subject = ' ALEGRA LABS APPLICANT EMAIL ';
		    
		  //   $from = $email_id;

		  //   $messages = 'Applicant Name :- '.$full_name."\r\n";
		  //   $messages .= 'Email Id :- '.$email_id."\r\n";
		  //   $messages .= 'Contact No.:- '.$phone_no."\r\n";
		  //   $messages .= 'Address :- '.$address."\r\n";
		  //   $messages .= 'Skills :- '.$skill."\r\n";
		  //   $messages .= 'Training Willing :- '.$training."\r\n";
		  //   $messages .= 'Why Hire him/her :- '.$hire_me_reason."\r\n";
		  //   $messages .= 'Discipline Grading :- '.$discipline."\r\n";
		  //   $messages .= 'Attendence Grading :- '.$working."\r\n";
		  //   $messages .= 'Resume Link :- '.$resumepath."\r\n";
		  //   $messages .= 'Policy Accepted :- '.$agree."\r\n";

			 // $mail = new PHPMailer();  // create a new object
		  //    $mail->IsSMTP(); // enable SMTP
		  //    $mail->SMTPDebug = 1;  // debugging: 1 = errors and messages, 2 = messages only
		  //    $mail->SMTPAuth = true;  // authentication enabled
		  //    $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
		  //    $mail->Host = 'smtp.gmail.com';
		  //    $mail->Port = 465; 
		  //    $mail->Username = "info@alegralabs.com";
		  //    $mail->Password = "XXXXXXXXXX";
		  //    $mail->SetFrom("info@alegralabs.com", "Jay J. Das");
		  //    $mail->Subject = $subject;
		  //    $mail->Body = $messages;
		  //    $mail->AddAddress($to);
		     
		  //    if(!$mail->Send()) {
		  //       $status = "fail";
		  //       $message = "There is a problem sending the mail to admin";

		  //       $to = $email_id;
			 //    $subject = ' ALEGRA LABS APPLICANT EMAIL ';
			 //    $from = 'jay@alegralabs.com';

			 //    $message = 'We received your application please wait for interview call.';

			 //    if(!$mail->Send()) {
			 //        $status = "fail";
			 //        $message = "There is a problem sending the mail to user";
			 //    }else{
			 //        $status = "success";
			 //        $message = "We have received your message. Thank you!";
			 //    }

		  //    } else {
		  //       $status = "success";
		  //       $message = "We have received your message. Thank you!";
		  //    }
    }else{
     	$status = "fail";
        $message = "Please try again later.";
    }
}

$obj = new stdClass();  // creation of object
$obj->status = $status;
$obj->message = $message;
$obj->error = $error;
echo json_encode($obj);

?>