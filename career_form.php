<?php 
require_once 'database.php';

require("PHPMailer_5.2.0/class.phpmailer.php");
date_default_timezone_set('Asia/Kolkata');



function sanitize($dirty){
    global $db;
    return trim(mysqli_real_escape_string($db, $dirty));
}

function get_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

$error = $year = array();
$status = $message = "";

$site_path  = "http://".$_SERVER['SERVER_NAME']."/career";

$career_submit = ( isset($_POST['career_submit']) ?  $_POST['career_submit'] : '' );

$singleskill = (isset($_POST['skill_select']) ? $_POST['skill_select'] : '' );
$singleskillyr = (isset($_POST['yr-expert_select']) ? $_POST['yr-expert_select'] : '' );

$full_name = ( isset($_POST['full_name']) ? $_POST['full_name'] : '' );
$email_id = (isset($_POST['email_id']) ? $_POST['email_id'] : '' );
$phone_no = (isset($_POST['phone_no']) ? $_POST['phone_no'] : '' );
$address = (isset($_POST['address']) ? $_POST['address'] : '' );
$skill = (isset($_POST['skill_array']) ? $_POST['skill_array'] : $singleskill );
$skillarr = explode(',', $skill);

$year = (isset($_POST['year_array']) ? $_POST['year_array'] : $singleskillyr );
$yeararr = explode(',', $year);

$training = (isset($_POST['training']) ? $_POST['training'] : '' );
$hire_me_reason = (isset($_POST['hire_me_reason']) ? $_POST['hire_me_reason'] : '' );
$discipline = (isset($_POST['discipline']) ? $_POST['discipline'] : '' );
$working = (isset($_POST['working']) ? $_POST['working'] : '' );
$agree = (isset($_POST['policy']) ? $_POST['policy'] : '' );
$resumeName =  (isset($_FILES['resume']['name']) ? $_FILES['resume']['name'] : '' );
$resumeSize = ( isset($_FILES['resume']['size']) ? $_FILES['resume']['size'] : '' );
$tmpLoc =  ( isset($_FILES['resume']['tmp_name']) ? $_FILES['resume']['tmp_name'] : '' );


if ($_POST) {

	if (empty($full_name)) {
		$error['full_name'] = 'Enter your full name.';
	}else{
		if( !preg_match('/^[a-zA-Z\s.]{3,255}$/', $full_name) ) {
	      $error['full_name'] = 'Name should be of max. 255 characters. Only Alphabets, space and dot allowed.';
	    }
	}


	if (empty($email_id)) {
		$error['email_id'] = 'Enter your email address.';
	}else{
		if (!filter_var($email_id, FILTER_VALIDATE_EMAIL)) {
	      $error['email_id'] = 'You must enter a valid email address.'; 
	    }else{
		    $emailquery = $db->query("SELECT * FROM recruit_application WHERE email = '$email_id' ");
		    $emailcount = mysqli_num_rows($emailquery);
		    
		    if ( $emailcount > 0) {
		    	$error['email_id'] = 'This email is already exist, please use another email id.';
		    }
	    }
	}

	if (empty($phone_no)) {
		$error['phone_no'] = 'Enter your phone number.';
	}else{
		if ( strlen($phone_no) > 15 ) {
			$error['phone_no'] = 'Phone no length must be max 15 digits';		
		}else{
			if( !preg_match('/^[+0-9*]{6,15}$/', $phone_no) ) {
		      $error['phone_no'] = 'Invalid phone number.';
		    }else{
		    	$phonequery = $db->query("SELECT * FROM recruit_application WHERE phone = '$phone_no' ");
			    $phonecount = mysqli_num_rows($phonequery);		    
			    if ( $phonecount > 0) {
			    	$error['phone_no'] = 'This phone/mobile number is already exist, please use another phone/mobile number.';
			    }
		    }
		}

	}

	if (empty($address)) {
		$error['address'] = 'Enter your full address details.';	
	}else{
		if (strlen($address) > 250 ) {
			$error['address'] = 'Enter maximum 250 characters';
		}
	}


	if (empty($skillarr) || empty($yeararr)) {
		$error['skills'] = 'Please select your skills.';
	}else{
		for ($i=0; $i < count($yeararr); $i++) { 
			if ($yeararr[$i] > 15) {
				$error['skills'] = 'Please add your skills first.';
			}			
		}
		for ($i=0; $i < count($skillarr); $i++) { 
			$allowed = array('Linux','C','C++','Objective C (for iOS)','Swift (for iOS)','GO','Scala','Java (for Android)','React JS','React Native','Ionic-Flutter for Hybrid Mobile App','Artificial Inteligence','Machine Learning/ Deep Learning','Blockchain','Vue','Angular','Node','HTML5','CSS3','Twitter Bootstrap','PHP','Python','Ruby','jQuery','Javascript','Restful API (XML/JSON)','MySQL','PgSQL','Apache Storm','Firebase','Mongo DB','Couch DB','Hadoop','Wordpress','WooCommerce','Magento','Drupal','Sales Force','Shopify','SAP','Zend MVC Framework','Yii MVC Framework','Laravel MVC Framework','CodeIgnitor MVC Framework','Cake PHP MVC Framework','Symphony MVC Framework','Smarty Templates');
			if (!in_array($skillarr[$i], $allowed)) {
				$error['skills'] = 'Please add your skills first.';
			}
		}
	}

	if (empty($training)) {
		$error['training'] = "Please select training condition";
	}

	if ($career_submit == 0 ) {
		if (empty($singleskill) || empty($singleskillyr)) {
			$error['singleskills'] = 'Please select your skill and years.';
		}
	}	

	if (empty($hire_me_reason)) {
		$error['hire_me_reason'] = 'Please fill why company hire you.';
	}else{
		if (strlen($hire_me_reason) > 510 ) {
			$error['hire_me_reason'] = 'Enter maximum 500 characters';
		}
	}

	if (empty($discipline)) {
		$error['discipline'] = 'Select your discipline grade.';	
	}else{
		if ($discipline == 0) {
			$error['discipline'] = 'Discipline grade cant be zoro.';	
		}
	}

	if (empty($working)) {
		$error['working'] = 'Select your attendence and working grade.';	
	}else{
		if ($working == 0) {
			$error['working'] = 'Attendence and working grade cant be zoro.';	
		}
	}

	if (empty($agree)) {
		$error['agree'] = 'You must agree to work for at least 2 years.';	
	}

	if (empty($resumeName)) {
		$error['resume'] = 'Upload your resume.';		
	}else{
		$resumenamearray = explode('.', $resumeName);
		$fileName =( isset($resumenamearray[0]) ? $resumenamearray[0] : '' );
		$fileExt = ( isset($resumenamearray[1]) ? $resumenamearray[1] : '' );
		$allowed = array('txt','doc','docx','pdf');
		if (!in_array($fileExt, $allowed)) {
			$error['resume'] = 'The file extension must be a txt, doc, docx, or pdf';
		}
	}

	if (!empty($error)) {
		$status = "fail";
	    $message = "Please check your fields properly then submit the form.";
		
	}else{

		$t = microtime(true);
	    $micro = sprintf("%06d",($t - floor($t)) * 1000000);
	    $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
	    $date = $d->format('YmdHisu');

	    $newResumeName = $date.$resumeName;

		$uploadPath =  'resume_application/'.$newResumeName;
		$resumepath = $site_path.'/resume_application/'.$newResumeName;

		move_uploaded_file($tmpLoc, $uploadPath);

		$full_name = sanitize($full_name); 
		$email_id = sanitize($email_id); 
		$phone_no = sanitize($phone_no); 
		
		$address = sanitize($address); 
		$hire_me_reason = sanitize($hire_me_reason);
		$discipline = sanitize($discipline);
		$working = sanitize($working);


		$skillsFinal = implode (",", $skillarr);
		$yearFinal = implode (",", $yeararr);

		$ip = get_ip();

		$insertApplication = "INSERT INTO recruit_application ( full_name, email, phone, address, skills, years, training, why_hire_me, discipline_grade, working_grade, resume, agreement, ip_address ) values ('$full_name','$email_id','$phone_no','$address','$skillsFinal','$yearFinal','$training','$hire_me_reason','$discipline','$working','$newResumeName','$agree','$ip' ) " ;

		$applicationQuery = $db->query($insertApplication);

		if ($applicationQuery) {
			$status = "success";
		    $message = "We have recieve your application, Thank You!.";
		    mysqli_close($db);

			 //    $to = 'jay@alegralabs.com';
			 //    $touser = $email_id;
			 //    $subject = ' ALEGRA LABS APPLICANT EMAIL ';
			    
			 //    $from = $email_id;

			 //    $messages = 'Applicant Name :- '.$full_name."\r\n";
			 //    $messages .= 'Email Id :- '.$email_id."\r\n";
			 //    $messages .= 'Contact No.:- '.$phone_no."\r\n";
			 //    $messages .= 'Address :- '.$address."\r\n";
			 //    $messages .= 'Skills :- '.$skill."\r\n";
			 //    $messages .= 'Training Willing :- '.$training."\r\n";
			 //    $messages .= 'Why Hire him/her :- '.$hire_me_reason."\r\n";
			 //    $messages .= 'Discipline Grading :- '.$discipline."\r\n";
			 //    $messages .= 'Attendence Grading :- '.$working."\r\n";
			 //    $messages .= 'Resume Link :- '.$resumepath."\n";
			 //    $messages .= 'Policy Accepted :- '.$agree."\r\n";

			 //    $message2  = 'Dear '.$full_name."\n\n";
			 //    $message2 .= 'We have received your application. Please be aware that we will not reply to unsuccessful applicants.'."\n".'Please be assured that your application will be reviewed promptly and if it is of interest, you will be contacted within 7 working days.'."\n\n";
			 //    $message2 .= 'Thank You'."\n";
			 //    $message2 .= 'Alegra Labs';
			    
			    
				// $subject2 = ' ALEGRA LABS APPLICANT EMAIL ';


				// $mail = new PHPMailer();  // create a new object
				// $mail->IsSMTP(); // enable SMTP
				// $mail->SMTPDebug = 1;  // debugging: 1 = errors and messages, 2 = messages only
				// $mail->SMTPAuth = true;  // authentication enabled
				// $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
				// $mail->Host = 'smtp.gmail.com';
				// $mail->Port = 465; 
				// $mail->Username = "info@alegralabs.com";
				// $mail->Password = "xxxxxxxxxxxx";
				// $mail->SetFrom("info@alegralabs.com", "Jay J. Das");
				// $mail->Subject = $subject;
				// $mail->Body = $messages;
				// $mail->AddAddress($to);
				// $mail->Send();

				// $mail->ClearAllRecipients();

				// $mail->Subject = $subject2;
				// $mail->Body = $message2;
				// $mail->AddAddress($touser);
			     
				// if(!$mail->Send()) {
				// 	$status = "fail";
				// 	$message = "There is a problem sending the mail to admin";
				// } else {
				// 	$status = "success";
				// 	$message = "We have received your message. Thank you!";					   
				// }
	    }else{
	     	$status = "fail";
	        $message = "Please try again later : Error - > D1";
	    }
	}
}

$obj = new stdClass();  // creation of object
$obj->status = $status;
$obj->message = $message;
$obj->error = $error;

if ($career_submit == 1 ) {
	echo json_encode($obj);	
}else{
	echo "";
}

?>